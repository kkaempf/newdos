#!/usr/bin/ruby
#
# newdos.rb
#
# trsdos / newdos / gdos file manipulator
#
#

# trs-80 standard
SECSIZE = 256

FORMATS = {
  # pdrive table at 0xc00 in image
#
#              SP SEK EIB       SBIV AEIV      SWZ           # GDOS
#   ddsl       tc spt gpl       ddsl ddga      TSR           # NEWDOS
#0  18   30 53 28 12  03  00 03 18   03  00 00 03  04 04 04
#1  18   30 53 28 12  03  00 03 18   03  00 00 03  04 04 04
#2  18   30 53 28 12  03  00 03 18   03  00 00 03  04 04 04
#3  18   30 53 28 12  03  00 03 18   03  00 00 03  04 04 04
# TI=CK,TD=E,SP=40,SEK=18,SWZ=3,EIB=3,SBIV=24, AEIV=3
#
#4  11   28 33 28 12  06  00 11 11    02  00 00 03 04 10 04
# TI=CM,TD=E,SP=40,SEK=18,SWZ=3,EIB=6,SBIV=17,AEIV=2
#
#   ddsl       tc spt gpl       ddsl ddga      TSR           # NEWDOS
#5  11   46 53 27 12  02  00 03 11    02  00 00 03 04 04 04
# TI=CK,TD=E,SP=39,SEK=18,SWZ=3,EIB=2,SBIV=17,AEIV=2
#
#6  11   50 06 50 0A  02  00 00 11    02  00 00 02 01 00 00
# TI=A,TD=A,SP=80,SEK=10,SWZ=2,EIB=2,SBIV=17, AEIV=2
#
#7  18   30 53 28 12  03  00 03 18    03  00 00 03 04 04 04
# TI=CK,TD=E,SP=40,SEK=18,SWZ=3,EIB=3,SBIV=24, AEIV=3
#
#8  18   30 53 28 24  06  00 43 18    06  00 00 03 04 04 06
# TI=CK,TD=G,SP=40,SEK=36,SWZ=3,EIB=6,SBIV=24, AEIV=6
#
#9  30   60 53 50 24  06  00 43 30    06  00 00 03 04 04 06
# TI=CK,TD=G,SP=80,SEK=36,SWZ=3,EIB=6,SBIV=48, AEIV=6

#    ddsl         tc spt gpl       ddsl ddga      TSR           # NEWDOS
                
#GDOS  30   60 54 50 24  06  80 43 30    06  00 00 00 10 04 06
# TI=EK,TD=G,SP=80,SEK=36,SWZ=0,EIB=6,SBIV=48, AEIV=6

  # 5.25" DSDD, Track0 in FM, 80 Tracks, 2 x 18 sectors per track
  # 6 granules per lump
  # directory starts at lump 48
  # directory spans 6 granules
  # offset: computed by hxcfe when converting from .hfe to .img via RAW_LOADER
  #
  # granule == 5 sectors
  # lump == 30 sectors (granule * GPL)
  # directory starts at lump(30) x DDSL(48) = 1440 (1440 / SPT(36) = Track 40)
  "NEWDOS 80DSDD"   => { model:1, entrysize: 32, tc: 80, spt: 36, gpl: 6, ddsl: 48, ddga: 6, sides: 2, reserved: 1, offset: 0xf00 },
  "NEWDOS S80DSDD"  => { model:1, entrysize: 32, tc: 80, spt: 36, gpl: 6, ddsl: 48, ddga: 6, sides: 2, reserved: 1, offset: 10*SECSIZE },
  "NEWDOS S81DSDD"  => { model:1, entrysize: 32, tc: 81, spt: 36, gpl: 6, ddsl: 48, ddga: 6, sides: 2, reserved: 1, offset: 10*SECSIZE },
  "NEWDOS S40SSDD"  => { model:1, entrysize: 32, tc: 40, spt: 18, gpl: 3, ddsl: 17, ddga: 2, sides: 1, reserved: 1, offset: 10*SECSIZE },
  "NEWDOS S41SSDD"  => { model:1, entrysize: 32, tc: 41, spt: 18, gpl: 3, ddsl: 24, ddga: 3, sides: 1, reserved: 1, offset: 10*SECSIZE },
  "NEWDOS S41DSDD"  => { model:1, entrysize: 32, tc: 41, spt: 36, gpl: 3, ddsl: 24, ddga: 3, sides: 2, reserved: 1, offset: 10*SECSIZE },
  "TRSDOS S80DSDD"  => { model:1, entrysize: 32, tc: 80, spt: 36, gpl: 6, ddsl: 48, ddga: 5, sides: 2, reserved: 1, offset: 10*SECSIZE },
  "TRSDOS S40SSDD"  => { model:1, entrysize: 32, tc: 40, spt: 18, gpl: 2, ddsl: 17, ddga: 1, sides: 1, reserved: 1, offset: 10*SECSIZE },
  # Model 3 TRSDOS
  # TI=AM,TD=E,TC=40,SPT=18,TSR=3,GPL=6,DDSL=17,DDGA=2
  "TRSDOS M3"       => { model:3, entrysize: 48, tc: 40, spt: 18, gpl: 6, ddsl: 17, ddga: 6, sides: 1, reserved: 1, offset: 0 },
  "40SSDD"          => { model:1, entrysize: 32, tc: 40, spt: 18, gpl: 2, ddsl: 17, ddga: 2, sides: 1, reserved: 0, offset: 0 },
  "40SSSD"          => { model:1, entrysize: 32, tc: 40, spt: 10, gpl: 2, ddsl: 17, ddga: 2, sides: 1, reserved: 0, offset: 0 },
  "35SSSD"          => { model:1, entrysize: 32, tc: 35, spt: 10, gpl: 2, ddsl: 17, ddga: 2, sides: 1, reserved: 0, offset: 0 },
  "DOSPLUS S35SSDD" => { model:1, entrysize: 32, tc: 35, spt: 18, gpl: 2, ddsl: 29, ddga: 2, sides: 1, reserved: 1, offset: 10*SECSIZE },
  "DOSPLUS S40SSDD" => { model:1, entrysize: 32, tc: 40, spt: 18, gpl: 2, ddsl: 29, ddga: 2, sides: 1, reserved: 1, offset: 10*SECSIZE },
  "DOSPLUS 40SSDD"  => { model:1, entrysize: 32, tc: 40, spt: 18, gpl: 2, ddsl: 36, ddga: 2, sides: 1, reserved: 0, offset: 0 },
  # Colour Genie, 41 Tracks
  # TI=CHK,TD=G,SP=80,SEK=36,SWZ=1,EIB=6,SBIV=48,AEIV=6
  "CGENIE"          => { model:1, entrysize: 32, tc: 41, spt: 18, gpl: 3, ddsl: 48, ddga: 6, sides: 2, reserved: 1, offset: 0 }
}

image = cmd = name = ext = nil

#--------------------------------------------------------------------------

class Format
  attr_reader :name
  def initialize name=nil, debug=nil
    name ||= ENV['FORMAT']
    self.format = name if name
    STDERR.puts "Format: #{name||'auto'}" if debug
  end
  def secpergranule
    case self.model
    when 1 then 5
    when 3 then 3
    else
      STDERR.puts "Unknown model #{self.model}"
      exit 1
    end
  end

  def find_format size
    diff = nil
    best_name = nil
    FORMATS.each do |name, fmt|
      @name = name
      @fmt = fmt
      if self.size == size
        puts "Assuming #{@name}"
        break
      else
        d = self.size - size
        if d < 0 # take absolute value
          d = -d
        end
        if diff
          if d < diff
            diff = d
            best_name = name
          end
        else
          diff = d
          best_name = name
        end
      end
      @name = nil
      @fmt = nil
    end
    unless @name
      puts "Best format #{best_name}, diff #{diff/SECSIZE} sectors"
    end
    return @name
  end
  def format= name
    @fmt = FORMATS[name]
    @name = name if @fmt
  end
  def to_s
    @name || "Format<unknown>"
  end
  def self.list
    FORMATS.each do |name,hash|
      puts "#{name}\t#{hash.inspect}"
    end
  end
  #
  # compute total size of .img file in bytes
  #
  def size
    (self.tc - self.reserved) * self.spt * SECSIZE + self.offset
  end
  #
  # convert granules to bytes
  #
  def granules2bytes granules
    bytes = granules * self.secpergranule * SECSIZE
    STDERR.puts "granules2bytes #{granules} grans * #{self.secpergranule} sec per gran * 256 SECSIZE = #{bytes}" if debug
    bytes
  end
  #
  # convert lump to granule
  #
  def lump2granule lump
    STDERR.puts "lump2granule #{lump} lump * #{self.gpl} granperlump = #{lump*self.gpl}" if debug
    lump * self.gpl
  end
  #
  # start byte of directory in disk image
  #
  def dirstart
    STDERR.puts "dirstart granules2bytes(lump2granule(self.ddsl #{self.ddsl})) + self.offset #{self.offset}" if debug
    start = granules2bytes(lump2granule(self.ddsl)) + self.offset
    STDERR.puts "dirstart track #{start/(SECSIZE * self.spt)}" if debug
    start
  end

  def method_missing name, *args
#    STDERR.puts("#{name.inspect} => #{@fmt[name].inspect}")
    @fmt[name] rescue nil
  end
end

#
# simple error/usage
#
def usage msg=nil
  if msg
    STDERR.puts "Error: #{msg}"
  else
    STDERR.puts "Usage: newdos <cmd> [<image> [<options> ...]"
    STDERR.puts "  <image> - .img file (from hxcfe RAW_LOADER)"
    STDERR.puts "  <cmd> - ls       - list directory"
    STDERR.puts "                     options: -l - long list"
    STDERR.puts "          cat      - read file(s) (to stdout)"
    STDERR.puts "          copy     - copy file(s)"
    STDERR.puts "          formats  - print built-in formats"
    STDERR.puts "                     setenv FORMAT to select one"
    STDERR.puts "          dirpos   - print offset of directory"
    STDERR.puts "          repair   - print 'dd' commands to repair image"
    STDERR.puts
    STDERR.puts "All command can take a name and an extension pattern with '*' and '?'"
    STDERR.puts
    STDERR.puts "Examples:"
    STDERR.puts "newdos ls disk.img -l          long directory list"
    STDERR.puts "newdos ls disk.img -l k\\*      long directory list, only files starting with k"
    STDERR.puts "newdos ls disk.img -l a??      long directory list, only files starting with a and 1-3 charaters"
    STDERR.puts "newdos copy disk.img doc txt   copy 'DOC/TXT' to 'doc.txt'"
    STDERR.puts "newdos formats                 print all built-in formats"
    STDERR.puts "newdos dirpos                  print directory offset as hex"
  end
  exit 1
end


#
# one entry (incl. overflow) of the directory
# See page 5-7 of https://archive.org/download/NewDOS-80_v2.5_19xx_Apparat_Inc/NewDOS-80_v2.5_19xx_Apparat_Inc.pdf
#
class DirEntry
  attr_reader :name, :ext, :type, :update_hash, :access_hash, :extends, :eof, :lrl
  # f: file handle of image
  # num: entry's relative number within directory (0..n)
  # fmt: Format
  def initialize f, num, fmt
    @num = num
    @fmt = fmt
    # _d_irectory _entry_ _c_ode
    @dec = num2dec num
    entry = read_fde f, @dec
    # FPDE - file primary directory entry
    @type, @alloc, @unused, @eof_low, @lrl = entry.unpack("C5")
    return unless self.used?
    return if self.fxde?
    @name = entry[5,8]
    @ext = entry[13,3]
    @update_hash, @access_hash = entry[16,4].unpack("v2")
    @eof_mid, @eof_high = entry[20,2].unpack("C2")
    @extends = Array.new
    unpack_extends f, @dec, entry
#    puts "#{self} : #{@extends.inspect}"
  end
  #
  # relative entry number to directory entry code
  #
  def num2dec num
    case @fmt.model
    when 1 then
      sssss = num / (SECSIZE / @fmt.entrysize)
      rrr = num % (SECSIZE / @fmt.entrysize)
      (rrr << 5) + sssss
    when 3 then
      sssss = num / 5
      rrr = num % 5
      (rrr << 5) + sssss
    else
      STDERR.puts "Unknown model"
    end
  end
  #
  # unpack extend information from entry
  #
  def unpack_extends f, dec, entry
#    print "unpack_extends #{self} @ #{dec} "
    low = nil
    entry[22,10].unpack("C*").each do |e|
      if low.nil?
        low = e
      else
        case low
        when 0xff
          break # end of chain
        when 0xfe
          # e is DEC of FXDE
          add_fxde f, dec, e
        else
          @extends << [low, e]
        end
        low = nil
      end
    end
  end
  def to_s
    "#{self.filename} @ #{@num}"
  end
private
  #
  # read file descriptor entry (fde) by directory entry code (dec)
  def read_fde f, dec
    rrr = (dec & 0xE0) >> 5
    sssss = dec & 0x1f
    f.seek(@fmt.dirstart + (sssss+2)*SECSIZE + rrr * @fmt.entrysize)
    f.sysread(@fmt.entrysize)
  end
  #
  # add FXDE
  #
  def add_fxde f, parent, dec
    fxde = read_fde f, dec
    back = fxde[1,1].ord
    if back != parent
      STDERR.puts "Bad FXDE. Parent #{self} does not match backpointer #{back} @ #{dec}"
      exit 1
    end
    unpack_extends f, dec, fxde
  end
public
  def used?
    (@type & 0x10) == 0x10
  end
  def system?
    (@type & 0x40) == 0x40
  end
  def invisible?
    (@type & 0x08) == 0x08
  end
  def fxde?
    (@type & 0x90) == 0x90
  end
  def access_level
    @type & 0x03
  end
  def filename
    e = @ext.strip
    "#{@name.strip}" + (e.empty? ? "" : "/#{@ext.strip}")
  end
  def hashcode
    hc = 0
    (@name+@ext).unpack("C11").each do |a|
      a ^= hc
      a *= 2
      if a > 255
        a -= 256
        a += 1
      end
      hc = a
    end
    hc
  end
  #
  # file size in bytes
  #
  def size
    eof_sector = @eof_high * 256 + @eof_mid
    if @eof_low != 0
      eof_sector -= 1 # non-RBA - relative byte addressing
    end
    eof_sector * 256 + @eof_low
  end
  #
  # extend to start and length
  #
  def each
    @extends.each do |lump, high|
      startgran = (high & 0xe0) >> 5
      grancount = (high & 0x1f) + 1
      yield lump, startgran, grancount
    end
  end
  #
  # read file
  #
def read f
#    STDERR.puts "Reading #{self} - #{self.size} bytes"
    total = self.size
    self.each do |lump, start, count|
#      STDERR.puts "\t lump #{lump} + #{start} .. #{count}"
      gran = @fmt.lump2granule(lump) + start
      off = @fmt.granules2bytes(gran) + @fmt.offset
      size = @fmt.granules2bytes(count)
      if (size > total)
        size = total
      else
        total -= size
      end
#      STDERR.printf "\t gran %d, off %x, count %x\n", gran, off, size
      f.seek(off)
      yield f.sysread(size)
    end
  end
end

#
# the directory
#
class Dir
  attr_reader :name, :date, :gat
  def initialize f, fmt
    @f = f
    @fmt = fmt
    # read GAT
    # 0x00 .. 0x5f
    @f.seek(fmt.dirstart + 0)  # GAT position within directory
    @gat = @f.sysread(0x60).unpack("C96")
    # 0x60 .. 0xbf
    @lockout = @f.sysread(0x60).unpack("C96")
    @f.seek(@fmt.dirstart + 0xce)
    # 0xce - 0xcf
    @password = @f.sysread(2).unpack("v")
    # 0xd0 - 0xd7
    @name = @f.sysread(8)
    # 0xd8 - 0xdf
    @date = @f.sysread(8)
    # 0xe0 - 0xff
    @auto = @f.sysread(32)
    # read HIT (@dirstart + SECSIZE)
    @hit = @f.sysread(SECSIZE).unpack("C*")
  end

  def maxentries
    # subtract GAT and HIT
    case @fmt.model
    when 1 then
      (@fmt.granules2bytes(@fmt.ddga) - 2*SECSIZE) / @fmt.entrysize
    when 3 then
      # 5 entries per sector
      ((@fmt.granules2bytes(@fmt.ddga) - 2*SECSIZE) / SECSIZE) * 5
    else
      STDERR.puts "Unknown model"
    end
  end
  #
  # fill @entries as Array of DirEntry
  #
private
  def fill_entries
    entries = Array.new
    @free = 0
    num = 0 # directory entry number
    while (num < maxentries)
      entry = DirEntry.new @f, num, @fmt
      num += 1
      if entry.used?
        next if entry.fxde?
        entries << entry
      else
        @free += 1
      end
    end
    @entries = entries
  end
public
  #
  # number of free directory entries
  #
  def free
    fill_entries unless @free
    @free
  end
  #
  # all directory entries
  #
  def entries
    fill_entries unless @entries
    @entries
  end

  # list directory entries
  def ls options=nil, name="*", ext="*"
    puts "#{maxentries} possible dir entries" if options
    i = 0
    name = name || "*"
    ext = ext || "*"
    self.each(name, ext) do |entry|
      next unless entry.used?
      printf "%-12s", entry.filename
      i += 1
      if options
        printf "%6d bytes -- [%02x] ", entry.size, entry.hashcode
        entry.each do |lump, start, len|
          print "(lump #{lump}+#{start}..#{len})"
        end
        puts
      else
        if i % 4 == 0
          puts
        else
          print "\t"
        end
      end
    end
    unless options
      puts unless i % 4 == 0
    end
  end
  #
  # match name or ext
  #
  def match s, t, len
    sp = s.split("")
    tp = t.split("")
    while sp.size > len
      sp.pop
    end
    sp.each do |sc|
      st = tp.shift
      case sc
      when '*'
        return true
      when '?'
        next
      else
        return false unless sc.upcase == st
      end
    end
    st = tp.shift
    (st == " " || st.nil?) # return true if t exhausted
  end
  #
  # find entry
  #
  def each name, ext
    self.entries.each do |entry|
#      puts "match #{name.inspect}/#{ext.inspect} to #{entry}"
      if match(name, entry.name, 8)
        if match(ext, entry.ext, 3)
          yield entry
        end
      end
    end
  end
end

#
# Disk representation
#
class Disk
  attr_reader :dir

  def initialize image, fmt, debug
    usage unless image
    @fmt = Format.new fmt, debug
    unless File.readable?(image)
      usage "#{image} is not a readable image file"
    end
    @image = image
    actual_size = File.size(image)
    unless @fmt.name
      @fmt.find_format actual_size
      unless @fmt.name
        STDERR.puts "No format matches size #{actual_size}"
        exit 1
      end
    end
    expected_size = self.format.size
    diff = expected_size-actual_size
    if (actual_size < expected_size)
      STDERR.puts "Bad image file size"
      STDERR.puts "Expected #{expected_size}, actual #{actual_size}. Difference #{diff} (#{diff/SECSIZE} sectors)"
      exit 1
    end
    @f = File.open(image)
    @dir = Dir.new @f, @fmt
  end

  # name of disk
  def name
    @dir.name.strip
  end
  # format of disk
  def format
    @fmt
  end
  # date of disk
  def date
    @dir.date
  end
  #
  # usable disk size in bytes
  #
  def size
    @fmt.size - @fmt.offset
  end
  #
  # used in bytes
  #
  def used
    usedgranules = 0
    i = @fmt.reserved
    dir.gat.each do |g|
#      printf "gat[%d] = %02x, ", i, g
      bitcount = 0
      bitmask = 1
      while bitcount < @fmt.gpl
        if (g & bitmask) != 0
          usedgranules += 1
        end
        bitcount += 1
        bitmask *= 2
      end
      i += 1
      break if i == lumps
    end
    @fmt.granules2bytes usedgranules
  end
  #
  # total granules
  #
  def granules
    self.size / SECSIZE / @fmt.secpergranule
  end
  #
  # total lumps
  #
  def lumps
    granules / @fmt.gpl
  end
  #
  # compute free granules
  #
  def free
    freegranules = 0
    i = @fmt.reserved
    dir.gat.each do |g|
#      printf "gat[%d] = %02x, ", i, g
      bitcount = 0
      bitmask = 1
      while bitcount < @fmt.gpl
        if (g & bitmask) == 0
          freegranules += 1
        end
        bitcount += 1
        bitmask *= 2
      end
      i += 1
      break if i == lumps
    end
    freegranules
  end
  #
  # read a file
  #
  def read name, ext
#   puts "Disk.read #{name.inspect} #{ext.inspect}"
    @dir.each(name,ext) do |e|
      e.read(@f) do |block|
        yield block
      end
    end
  end
  #
  # copy a file
  #
  def copy name="*", ext="*"
    name = name || "*"
    ext = ext || "*"
    @dir.each(name, ext) do |e|
      ex = e.ext.strip.downcase
      newname = e.name.strip.downcase
      if (newname.empty?)
        puts "Target name is empty, setting to '_'"
        newname = "_"
      end
      unless ex.empty?
        newname = newname + "." + ex
      end
      puts "Copying #{e.filename} to #{newname}"
      while File.exist?(newname)
        puts "** #{newname} exists, copying to #{newname}.1"
        newname = newname + ".1"
      end
      File.open(newname, "w+") do |f|
        e.read(@f) do |chunk|
          f.write chunk
        end
      end
    end
  end
end

#==========================================================================

debug = nil
opt = nil

loop do
  arg = ARGV.shift
  break if arg.nil?
  if arg == "-d"
    STDERR.puts "** debug enabled **"
    debug = true
  elsif arg == "-l"
    opt = arg
  elsif cmd.nil?
    cmd = arg
  elsif image.nil?
    image = arg
  elsif name.nil?
    name = arg
  elsif ext.nil?
    ext = arg
  else
    STDERR.puts "Excess argument #{arg.inspect}"
    exit 1
  end
end

#puts "Command #{cmd.inspect}"
case cmd
when nil
  usage
when "dir", "ls"
  disk = Disk.new image, nil, debug
  fmt = disk.format

  puts "Name #{disk.name.inspect} from #{disk.date}"
  puts "#{disk.dir.free} of #{disk.dir.maxentries} entries free"
  free = disk.free
  puts "#{disk.size} bytes total, #{disk.granules} granules total, #{disk.lumps} lumps total"
  puts "#{disk.used} bytes used, #{free} granules free, #{fmt.granules2bytes free} bytes free"
  dir = disk.dir
  dir.ls opt, name, ext
when "cat"
  disk = Disk.new image, nil, debug

  disk.read(name, ext) do |file|
    print file
  end
when "copy", "cp"
  disk = Disk.new image, nil, debug

  disk.copy(name, ext)
when "formats"
  Format.list
when "dirpos"
  fmt = Format.new nil, debug
  unless fmt.name
    disk = Disk.new image, nil, debug
    fmt = disk.format
  end
  printf "Dir @ %d / 0x%x\n",  fmt.dirstart, fmt.dirstart
when "repair"
  # name: track
  # ext: sector
  fmt = Format.new nil, debug
  unless fmt.name
    STDERR.puts "Please set FORMAT env"
    exit 1
  end
  track = name.to_i
  sector = ext.to_i
  puts "Add 1 sector at track #{track}, sector #{sector} to #{image}"
  # compute sector offset
  reserved = fmt.offset / SECSIZE
  if reserved > 0 # this is track 0
    if track > 0
      track -= 1
    else
      STDERR.puts "Can't repair track 0 on a disk with #{reserved} reserved sectors"
      exit 1
    end
  end
  soff = reserved + track * fmt.spt + sector
  puts "dd if=#{image} of=#{image}.new bs=256 count=#{soff}"
  puts "dd if=/dev/zero of=#{image}.new bs=256 seek=#{soff} count=1"
  puts "dd if=#{image} of=#{image}.new bs=256 seek=#{soff+1} skip=#{soff}"
else
  usage "Unknown command #{cmd.inspect}"
end
